# Redirect-URL-Frontend

短網址轉跳頁面中轉頁面，這是一個初代概念的短網址轉跳頁面實作方式

## Demo

[Google Shortlink](https://redirecturl.netlify.app/?token=uNFgVo-f6)

[Youtube Shortlink](https://redirecturl.netlify.app/?token=GP4hNTbNA)

## Introduction

短網址是以行銷角度發想，行銷角度常會有需要推廣一頁式網站。因貼在社群網站上很有機會被舉報或封鎖。
短網址出現就可以解決這項問題，正式的 Domain 可以被隱藏起來。

## Use Technology & Library

- Vite + React
- Axios

## Features

- [x] 轉導至正確的目標網址
