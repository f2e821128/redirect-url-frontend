import axios from "axios";
import { useEffect, useState } from "react";

function App() {
  const [heading, setHeading] = useState("Decoding ...");
  const appVersion = APP_VERSION;

  const queryParams = new URLSearchParams(window.location.search);
  const token = queryParams.get("token");

  useEffect(() => {
    axios
      .post(`${import.meta.env.VITE_BACKEND_URL}/shortUrls/decode`, { shortUrl: token })
      .then(function (response) {
        const { destinationURL } = response.data;
        setHeading("Redirecting ...");
        window.location.assign(destinationURL);
      })
      .catch(function () {
        setHeading("Invaild Short URL");
      });
  }, []);

  return (
    <main>
      <h1>{heading}</h1>
      <small>{appVersion}</small>
    </main>
  );
}

export default App;
